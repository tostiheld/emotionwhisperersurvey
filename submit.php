<?php

include_once "common.php";

$insert_record_sql = "INSERT INTO result (participant_id, question, answer) VALUES (?, ?, ?)";

$participant_id = $_GET["participant_id"];
$results = json_decode(file_get_contents("php://input"), true);

foreach ($results as $question => $answer)
{
	$statement = $db_connection->prepare($insert_record_sql);
	$statement->bind_param("iss", $participant_id, $question, $answer);

	if (!$statement->execute()) {
		die($statement->error);
	}
	$statement->close();
}

$db_connection->close();

