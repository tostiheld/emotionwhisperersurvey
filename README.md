# Emotion Whisperer Survey

Based on [Experience Sampler](http://experiencesampler.com)

## Setup
- Install Cordova and the Android Sdk (or Android Studio)
- Execute the following commands inside this directory:

```shell
# check the installation
cordova requirements

# you might want to specify a version or another platform
# eg cordova platform add android@6.0.0 or cordova platform add ios
cordova platform add android

# prereq's from experience sampler
cordova plugin add cordova-plugin-dialogs
cordova plugin add cordova-plugin-console
cordova plugin add cordova-plugin-splashscreen
cordova plugin add cordova-plugin-vibration
cordova plugin add cordova-plugin-device
cordova plugin add de.appplant.cordova.plugin.local-notification

cordova run android # or another platform

```

