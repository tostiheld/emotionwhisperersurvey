/*EXPERIENCESAMPLER LICENSE
 The MIT License (MIT)
 Copyright (c) 2014-2015 Sabrina Thai & Elizabeth Page-Gould
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.*/

/* activate localStorage */
var localStore = window.localStorage;

var patterns = [
	[300, 50, 300, 50, 300],
	[100, 50, 100, 50, 100, 50, 100, 50, 100, 50, 100],
	[100, 70, 200, 70, 300, 70, 400 ],
	[100, 25, 100, 25, 100, 25, 100, 25, 100, 25, 100, 25, 100],
	[500, 50, 500],
	[100, 100, 100, 100, 100, 100, 100, 100, 100, 100],
	[100, 50, 100],
	[200, 100, 200, 100, 200, 100, 200],
	[450, 100, 450],
	[100, 200, 100, 200, 100, 200, 100]
];

var responses = {};

var questionProto = {
	"type": "vibration",
	"variableName": "vibration",
	"vibrationPattern": [],
	"questionPrompt": "Welke emotie vindt u het beste passen het trilpatroon dat u zojuist voelde?",
	"minResponse": 0,
	"maxResponse": 4,
	"labels": [
		{"label": "Blij"},
		{"label": "Verbaasd"},
		{"label": "Boos"},
		{"label": "Verdrietig"},
		{"label": "Bang"}
	]
};

/* surveyQuestion Model (This time, written in "JSON" format to interface more cleanly with Mustache) */
/* This is used to input the questions you would like to ask in your experience sampling questionnaire*/
var surveyQuestions = [
	{
		"type": "instructions",
		"variableName": "start",
		"questionPrompt": "Druk op start om te beginnen",
	},
];

// geslacht
// leeftijdscategorie
// geboorteland
// blind?
var metaQuestions = [
	{
		"type": "mult1",
		"variableName": "gender",
		"questionPrompt": "Wat is uw geslacht?",
		"minResponse": 0,
		"maxResponse": 2,
		"labels": [
			{"label": "Man"},
			{"label": "Vrouw"},
			{"label": "Neutraal/wil ik niet zeggen"}
		]
	},
	{
		"type": "mult1",
		"variableName": "age_category",
		"questionPrompt": "Wat is uw leeftijdscategorie?",
		"minResponse": 0,
		"maxResponse": 5,
		"labels": [
			{"label": "12 of jonger"},
			{"label": "17 of jonger"},
			{"label": "18 tot 27"},
			{"label": "28 tot 40"},
			{"label": "41 tot 60"},
			{"label": "61 en ouder"},
		]
	},
	{
		"type": "mult1",
		"variableName": "nationality",
		"questionPrompt": "Heeft u de afgelopen 10 jaar in Nederland gewoond?",
		"minResponse": 0,
		"maxResponse": 1,
		"labels": [
			{"label": "Ja"},
			{"label": "Nee"},
		]
	},
	{
		"type": "mult1",
		"variableName": "vision",
		"questionPrompt": "Is uw zicht minder dan 25%?",
		"minResponse": 0,
		"maxResponse": 1,
		"labels": [
			{"label": "Ja"},
			{"label": "Nee"},
		]
	}
]

/*These are the messages that are displayed at the end of the questionnaire*/
var lastPage = [
    /*input your last-page message*/
    {
        message: "Einde van de vragenlijst. Bedankt!"
    },
    /*input snooze last-page message*/
    {
        message: "Snooze message"
    }
];

/*Questions to set up participant notifications so that notifications are customized to participant's schedule*/
var participantSetup = [
];

/*Populate the view with data from surveyQuestion model*/
// Making mustache templates
//This line determines the number of questions in your participant setup
//Shout-out to Rebecca Grunberg for this great feature!
var NUMSETUPQS = participantSetup.length;

//This line tells ExperienceSampler which question in surveyQuestions is the snooze question
//If you choose not to use the snooze option, just comment it out
var SNOOZEQ = 0;
//This section of code creates the templates for all the question formats
var questionTmpl = "<p>{{{questionText}}}</p><ul>{{{buttons}}}</ul>";
var questionTextTmpl = "<small><b>Vraag nummer: {{questionNumber}}</b></small><br />{{{questionPrompt}}}";
var buttonTmpl = "<li><button id='{{id}}' value='{{value}}'>{{label}}</button></li>";
var textTmpl = "<li><textarea cols=50 rows=5 id='{{id}}'></textarea></li><li><button type='submit' value='Enter'>Enter</button></li>";
var checkListTmpl = "<li><input type='checkbox' id='{{id}}' value='{{value}}'>{{label}}</input></li>";
var instructionTmpl = "<li><button id='{{id}}' value = 'Next'>Start</button></li></ul><img src='img/logo-emotiefluisteraar.png' /></li><br /><img src='img/logo-htc.png' /><img src='img/logo-dcc.png' /><img src='img/logo-owfour.png' /><br /><button style='height:30px;width:200px;font-size:8pt;' onclick='app.exportJson()'>Resultaten exporteren</button>";
var sliderTmpl = "<li><input type='range' min='{{min}}' max='{{max}}' value='{{value}}' orient=vertical id='{{id}}' oninput='outputUpdate(value)'></input><output for='{{id}}' id='slider'>50</output><script>function outputUpdate(slidervalue){document.querySelector('#slider').value=slidervalue;}</script></li><li><button type='submit' value='Enter'>Enter</button></li>";
var datePickerTmpl = '<li><input id="{{id}}" data-format="DD-MM-YYYY" data-template="D MMM YYYY" name="date"><br /><br /></li><li><button type="submit" value="Enter">Enter</button></li><script>$(function(){$("input").combodate({firstItem: "name",minYear:2015, maxYear:2016});});</script>';
var dateAndTimePickerTmpl = '<li><input id="{{id}}" data-format="DD-MM-YYYY-HH-mm" data-template="D MMM YYYY  HH:mm" name="datetime24"><br /><br /></li><li><button type="submit" value="Enter">Enter</button></li><script>$(function(){$("input").combodate({firstItem: "name",minYear:2015, maxYear:2016});});</script>';
var timePickerTmpl = '<li><input id="{{id}}" data-format="HH:mm" data-template="HH : mm" name="time"><br /><br /></li><li><button type="submit" value="Enter">Enter</button></li><script>$(function(){$("input").combodate({firstItem: "name"});});</script>';
var lastPageTmpl = "<h3>{{message}}</h3><ul><li><button onclick='location.reload()'>Opnieuw</button></li></ul>";

var loaderTmpl = "<div class='loader'></div>";

//This line generates the unique key variable. You will not assign the value here, because you want it the value to
// change with each new questionnaire
var uniqueKey;

var app = {
    initialize: function () {
        this.bindEvents();
    },
    bindEvents: function () {
        document.addEventListener("deviceready", this.onDeviceReady, false);
        document.addEventListener("resume", this.onResume, false);
        document.addEventListener("pause", this.onPause, false);
    },
    onDeviceReady: function () {
        app.init();
    },

    onResume: function () {
        app.sampleParticipant();
    },

    onPause: function () {
        app.pauseEvents();
    },

	doVibration: function (pattern) {
		var totalTime = 0;
		for (var i = 0; i < pattern.length; i++) {
			totalTime += pattern[i];
		}
		navigator.vibrate(pattern);

		$("#question").fadeOut(1).delay(totalTime).fadeIn(1);
	},

    renderQuestion: function (question_index) {
        //First load the correct question from the JSON database
        var question;
        if (question_index <= -1) {
            question = participantSetup[question_index + NUMSETUPQS];
        }
        else {
            question = surveyQuestions[question_index];
        }
        var questionPrompt = question.questionPrompt;

		participantNumberText = Mustache.render(
			"<small><small>{{participant_id}}</small></small><br />", {
			participant_id: localStore.participant_id
		});
		question.questionText = participantNumberText;
        question.questionText += Mustache.render(questionTextTmpl, {
			questionPrompt: questionPrompt, 
			questionNumber: question_index
		});

        switch (question.type) {
			case 'vibration':
				app.doVibration(question.vibrationPattern);

            case 'mult1': // Rating scales
                question.buttons = "";
                var label_count = 0;
                for (var i = question.minResponse; i <= question.maxResponse; i++) {
                    var label = question.labels[label_count++].label;
                    question.buttons += Mustache.render(buttonTmpl, {
                        id: question.variableName + i,
                        value: i,
                        label: label
                    });
                }
                $("#question").html(Mustache.render(questionTmpl, question));
                $("#question ul li button").click(function () {
                    app.recordResponse($(this), question_index, question.type);
                });
				
				if (question.type === 'vibration') {
					$("#question").append("<br /><br /><button class='again'>Patroon herhalen</button");
					$(".again").click(function() {
						app.doVibration(question.vibrationPattern);
					});
				}

                break;
            case 'mult2': // Rating scales
                question.buttons = "";
                var label_count = 0;
                for (var j = question.maxResponse; j >= question.minResponse; j--) {
                    var label = question.labels[label_count++].label;
                    if (label.indexOf('NAME') >= 0) {
                        label = label.replace("NAME", function replacer() {
                            return name;
                        });
                    }
                    question.buttons += Mustache.render(buttonTmpl, {
                        id: question.variableName + j,
                        value: j,
                        label: label
                    });
                }
                $("#question").html(Mustache.render(questionTmpl, question));
                $("#question ul li button").click(function () {
                    app.recordResponse(this, question_index, question.type);
                });
                break;
            case 'checklist':
                question.buttons = "";
                var label_count = 0;
                var checkboxArray = [];
                for (var i = question.minResponse; i <= question.maxResponse; i++) {
                    var label = question.labels[label_count++].label;
                    if (label.indexOf('NAME') >= 0) {
                        label = label.replace("NAME", function replacer() {
                            return name;
                        });
                    }
                    question.buttons += Mustache.render(checkListTmpl, {
                        id: question.variableName + i,
                        value: i,
                        label: label
                    });
                }
                question.buttons += "<li><button type='submit' value='Enter'>Enter</button></li>";
                $("#question").html(Mustache.render(questionTmpl, question));
                $("#question ul li button").click(function () {
                    checkboxArray.push(question.variableName);
                    $.each($("input[type=checkbox]:checked"), function () {
                        checkboxArray.push($(this).val());
                    });
                    app.recordResponse(String(checkboxArray), question_index, question.type);
                });
                break;
            case 'slider':
                question.buttons = Mustache.render(sliderTmpl, {id: question.variableName + "1"}, {min: question.minResponse}, {max: question.maxResponse}, {value: (question.maxResponse) / 2});
                $("#question").html(Mustache.render(questionTmpl, question));
                var slider = [];
                $("#question ul li button").click(function () {
                    slider.push(question.variableName);
                    slider.push($("input[type=range]").val());
                    app.recordResponse(String(slider), question_index, question.type);
                });
                break;
            case 'instructions':
                question.buttons = Mustache.render(instructionTmpl, {id: question.variableName + "1"});
                $("#question").html(Mustache.render(questionTmpl, question));
                var instruction = [];
                $("#question ul li button").click(function () {
                    instruction.push(question.variableName);
                    instruction.push($(this).val());
                    app.recordResponse($(this), question_index, question.type);
                });
                break;
            case 'text': //default to open-ended text
                question.buttons = Mustache.render(textTmpl, {id: question.variableName + "1"});
                $("#question").html(Mustache.render(questionTmpl, question));
                $("#question ul li button").click(function () {
 				
				if (app.validateResponse($("textarea"))){
                    app.recordResponse($("textarea"), question_index, question.type);
                 }
                 else {
                     alert("Please enter something.");
                 }
                });
                break;
            case 'datePicker':
                question.buttons = Mustache.render(datePickerTmpl, {id: question.variableName + "1"});
                $("#question").html(Mustache.render(questionTmpl, question));
                var date, dateSplit, variableName = [], dateArray = [];
                $("#question ul li button").click(function () {
                    date = $("input").combodate('getValue');
                    dateArray.push(question.variableName);
                    dateArray.push(date);
                    app.recordResponse(String(dateArray), question_index, question.type);
                });
                break;
            case 'dateAndTimePicker':
                question.buttons = Mustache.render(dateAndTimePickerTmpl, {id: question.variableName + "1"});
                $("#question").html(Mustache.render(questionTmpl, question));
                var date, dateSplit, variableName = [], dateArray = [];
                $("#question ul li button").click(function () {
                    date = $("input").combodate('getValue');
                    dateArray.push(question.variableName);
                    dateArray.push(date);
                    app.recordResponse(String(dateArray), question_index, question.type);
                });
                break;
            case 'timePicker':
                question.buttons = Mustache.render(timePickerTmpl, {id: question.variableName + "1"});
                $("#question").html(Mustache.render(questionTmpl, question));
                var time, timeSplit, variableName = [], timeArray = [];
                $("#question ul li button").click(function () {
                    time = $("input").combodate('getValue');
                    timeArray.push(question.variableName);
                    timeArray.push(time);
                    app.recordResponse(String(timeArray), question_index, question.type);
                });
                break;
        }
    },

    renderLastPage: function (pageData, question_index) {
        $("#question").html(Mustache.render(lastPageTmpl, pageData));
        app.saveDataLastPage();
    },

    init: function () {
        //First, we assign a value to the unique key when we initialize ExperienceSampler
        uniqueKey = new Date().getTime();

		if (!localStore.participant_id) {
			localStore.participant_id = 1;
		}
		else {
			localStore.participant_id++;
		}

		for (var i = 0; i < patterns.length; i++) {
			var question = $.extend(true, {}, questionProto);
			question.vibrationPattern = patterns[i];
			question.variableName = "vibration" + i;
			surveyQuestions.push(question);
		}

		for (var i = 0; i < metaQuestions.length; i++)
		{
			surveyQuestions.push(metaQuestions[i]);
		}

		responses["time"] = Date.now();

		/*uniqueKey = new Date().getTime();
		localStore.uniqueKey = uniqueKey;
		var startTime = new Date(uniqueKey);
		var syear = startTime.getFullYear(), smonth = startTime.getMonth(), sday = startTime.getDate(), shours = startTime.getHours(), sminutes = startTime.getMinutes(), sseconds = startTime.getSeconds(), smilliseconds = startTime.getMilliseconds();
		localStore[uniqueKey + "_" + "startTime" + "_" + syear + "_" + smonth + "_" + sday + "_" + shours + "_" + sminutes + "_" + sseconds + "_" + smilliseconds] = 1;
		*/
		app.renderQuestion(0);
        
		/*localStore.snoozed = 0;*/
    },

    /* Record User Responses */
    recordResponse: function (button, count, type) {
		if (type != "instructions")
		{
			var response = button.val();
			var question = button.attr('id').slice(0,-1);

			responses[question] = response;
		}

        if (count < surveyQuestions.length - 1) {
			$("#question").html("");
			app.renderQuestion(count + 1);
        }
        else {
            app.renderLastPage(lastPage[0], count);
        }
        ;
    },

    /* Prepare for Resume and Store Data */
    /* Time stamps the current moment to determine how to resume */
    pauseEvents: function () {
        localStore.pause_time = new Date().getTime();
        localStore.uniqueKey = uniqueKey;
        app.saveData();
    },

    sampleParticipant: function () {/*
        var current_moment = new Date();
        var current_time = current_moment.getTime();

        if ((current_time - localStore.pause_time) > X) {
            uniqueKey = new Date().getTime();
            localStore.snoozed = 0;
            var startTime = new Date(uniqueKey);
            var syear = startTime.getFullYear(), smonth = startTime.getMonth(), sday = startTime.getDate(), shours = startTime.getHours(), sminutes = startTime.getMinutes(), sseconds = startTime.getSeconds(), smilliseconds = startTime.getMilliseconds();
            localStore[uniqueKey + "_" + "startTime" + "_" + syear + "_" + smonth + "_" + sday + "_" + shours + "_" + sminutes + "_" + sseconds + "_" + smilliseconds] = 1;
            app.renderQuestion(0);
        }
        else {
            uniqueKey = localStore.uniqueKey;
        }
        app.saveData();*/
    },

    saveDataLastPage: function () {
		var collectedResults = []
		if (localStore.results) {
			collectedResults = JSON.parse(localStore.results);
		}
		collectedResults.push(responses);
		localStore.results = JSON.stringify(collectedResults);
		results = {};

		/*for (var i = 0; i < collectedResults.length; i++) {
			currentResult = collectedResults.shift();
			$.ajax({
				type: 'POST',
				url: 'https://thomasschraven.nl/ddw/submit.php?participant_id=' + localStore.participant_id,
				crossDomain: true,
				contentType: "application/json; charset=utf-8",
    			dataType: "json",
				data: JSON.stringify(currentResult),
				crossDomain: true,
				success: function (result) {
					alert("Data verzonden");
				},
				error: function (request, error) {
					failed.push(currentResult);
					alert("Data verzenden mislukt");
				},
			});
		}

		if (failed.length > 0) {
			localStore.results = JSON.stringify(failed);
		}*/
    },

//uncomment this function to test data saving function (Stage 2 of Customization)
    saveData: function () {
//     $.ajax({
//            type: 'post',
//            url: 'server url (i.e., url for where to send the data)',
//            data: localStore,
//            crossDomain: true,
//            success: function (result) {
//            var pid = localStore.participant_id, snoozed = localStore.snoozed, uniqueKey = localStore.uniqueKey;
//            localStore.clear();
//            localStore.participant_id = pid;
//            localStore.snoozed = snoozed;
// 			  localStore.uniqueKey = uniqueKey;
//            },
//            error: function (request, error) {console.log(error);}
//            });
    },

    scheduleNotifs: function () {

    },

    snoozeNotif: function () {
    },
    validateResponse: function (data) {
        var text = data.val();
        if (text === "") {
            return false;
        } else {
            return true;
        }
    },

	exportJson: function() {
		var blob = new Blob([ localStore.results ], { type: "text/plain;charset=utf-8" });
		app.requestFileSystem("export" + Date.now() + ".json", blob);
	},

	requestFileSystem: function(filename, blob) {
		window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fs) {
			fs.root.getFile(filename, { create: true, exclusive: false }, function(fileEntry) {
				console.log("write file");
				app.writeFile(fileEntry, blob);
			}, function onErrorCreateFile(error) {});
		}, function onErrorLoadFs(error) {});
	},

	writeFile: function(fileEntry, blob) {
		// Create a FileWriter object for our FileEntry
		fileEntry.createWriter(function(fileWriter) {
				fileWriter.onwriteend = function() {
				app.readFile(fileEntry);
			};

			fileWriter.onerror = function(error) {};

			// If data object is not passed in,
			// create a new Blob instead.
			fileWriter.write(blob);
		});
	},

	readFile: function(fileEntry) {
		fileEntry.file(function(file) {
			var reader = new FileReader();

			reader.onloadend = function() {
				app.shareFile(fileEntry.nativeURL);
		};

		reader.readAsText(file);

		}, function(error) {
			console.log(error);
		});
	},

	shareFile: function(fullPath) {
		console.log(fullPath);
		var options = {
			files: [fullPath],
			chooserTitle: 'Select an app'
		};
		window.plugins.socialsharing.shareWithOptions(options, function (result) {
			console.log(result);
			}, function (msg) {
				console.log(msg);
		});
	},
};

