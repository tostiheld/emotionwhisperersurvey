<?php PHP_SAPI === 'cli' or die('This script is intended for cli use only');

include_once "common.php";

$pattern_table_sql = "CREATE TABLE pattern
  (
     id           INT UNSIGNED PRIMARY KEY NOT NULL,
     pattern_json VARCHAR(2048) NOT NULL
  );
";

$results_table_sql = "CREATE TABLE result
  (
     participant_id INT UNSIGNED NOT NULL,
     question       VARCHAR(1024) NOT NULL,
	 answer         VARCHAR(1024) NOT NULL,
	 time           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
  ); 
";

/*if ($db_connection->query($pattern_table_sql) !== true)
{
	echo "Failed to create pattern table\n";
}
else*/
if ($db_connection->query($results_table_sql) !== true)
{
	echo "Failed to create results table\n$db_connection->error\n";
}

